# background-fm
Scripts to create backgrounds based on your currently playing music/top albums in the last month. 

Uses mpris to determine if a song is playing, and retrieve album art unless you are using spotify where it uses spotify's api to get a higher resolution image.
Also includes a script for when a song is not playing to fall back to top albums of the month based on your listens from the lastfm api.
